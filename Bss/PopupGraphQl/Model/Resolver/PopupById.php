<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_PopupGraphQl
 * @author     Extension Team
 * @copyright  Copyright (c) 2022 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\PopupGraphQl\Model\Resolver;

use Bss\Popup\Model\PopupFactory;
use Bss\Popup\Model\ResourceModel\Layout\CollectionFactory;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

class PopupById implements ResolverInterface
{
    /**
     * @var PopupFactory
     */
    private PopupFactory $popupFactory;

    /**
     * @var CollectionFactory
     */
    private CollectionFactory $collectionFactory;

    /**
     * @param PopupFactory $popupFactory
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        PopupFactory $popupFactory,
        CollectionFactory $collectionFactory
    ) {
        $this->popupFactory = $popupFactory;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        if (empty($args['popup_id'])) {
            throw new GraphQlInputException(__('Invalid parameter list.'));
        }
        $popupId = $args['popup_id'];
        $popupData = $this->popupFactory->create()->load($popupId)->getData();
        if (empty($popupData)) {
            throw new GraphQlInputException(__('Popup ID is not exist'));
        }
        $layoutData = [];
        $collection = $this->collectionFactory->create()->addFieldToFilter('popup_id', $popupId);
        foreach ($collection as $layout) {
            $layoutData[] = [
                'layout_id' => $layout['layout_id'],
                'page_group' => $layout['page_group'],
                'layout_handle' => $layout['layout_handle'],
                'page_for' => $layout['page_for'],
                'entities' => $layout['entities']
            ];
        }
        $popupData['layout'] = $layoutData;
        return $popupData;
    }
}
