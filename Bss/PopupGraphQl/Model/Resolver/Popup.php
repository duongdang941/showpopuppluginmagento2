<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_PopupGraphQl
 * @author     Extension Team
 * @copyright  Copyright (c) 2022 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\PopupGraphQl\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Bss\Popup\Model\HandleLayout;
use Bss\Popup\Model\PopupFactory;

class Popup implements ResolverInterface
{
    /**
     * @var HandleLayout $handleLayout
     */
    private $handleLayout;

    /**
     * @var PopupFactory
     */
    private PopupFactory $popupFactory;

    /**
     * @param HandleLayout $handleLayout
     * @param PopupFactory $popupFactory
     */
    public function __construct(
        HandleLayout $handleLayout,
        PopupFactory $popupFactory
    ) {
        $this->handleLayout = $handleLayout;
        $this->popupFactory = $popupFactory;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        if (empty($args['handleList']) || !isset($args['storeId'])) {
            throw new GraphQlInputException(__('Invalid parameter list.'));
        }
        $handleList = $args['handleList'];
        $storeId = $args['storeId'];
        $customerGroupId = $context->getExtensionAttributes()->getCustomerGroupId();
        $popupId = $this->handleLayout->getPopupId($handleList, $storeId, $customerGroupId);
        if (!$popupId) {
            throw new GraphQlInputException(__('Not exist popup'));
        }
        return $this->popupFactory->create()->load($popupId)->getData();
    }
}
