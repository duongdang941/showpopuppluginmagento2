<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_PopupGraphQl
 * @author     Extension Team
 * @copyright  Copyright (c) 2022 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */

namespace Bss\PopupGraphQl\Model\Resolver;

use Bss\Popup\Model\Config;
use Magento\Framework\GraphQl\Config\Element\Field as FieldAlias;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

/**
 * Use get config in Amin/Configuration/Bss/AjaxCart use graphql
 */
class GetConfigOutput implements ResolverInterface
{
    /**
     * @var Config
     */
    protected Config $config;

    /**
     * Constructor
     *
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * Get config and return output
     *
     * @param FieldAlias $field
     * @param ContextInterface $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return array
     * @throws GraphQlInputException
     */
    public function resolve(
        FieldAlias  $field,
        $context,
        ResolveInfo $info,
        array       $value = null,
        array       $args = null
    ): array {
        //Check is account admin
        if ($context->getUserType() != 2) {
            throw new GraphQlInputException(__("You are not authorized to get config"));
        }
        return [
            "enable" => $this->config->isEnable()
        ];
    }
}
